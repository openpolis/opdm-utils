"""Integration tests for :py:mod:`opdm_utils.wikimedia` module."""
import json
import unittest

from opdm_utils import wikimedia

ENDPOINT = "https://en.wikipedia.org/w/api.php"


class TestMediaWikiAPIWithIntegration(unittest.TestCase):
    """Test class for :py:class:`opdm_utils.wikimedia.MediaWikiAPI` class against the english Wikipedia API."""

    def test_query(self):
        """Test the :py:meth:`opdm_utils.wikimedia.MediaWikiAPI.query` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        content = api.query(
            prop="revisions",
            meta="siteinfo",
            titles="Main Page",
            rvprop="user|comment",
            format="json",
        )
        content = json.loads(content)
        self.assertIsNotNone(content)

    def test_parse(self):
        """Test the :py:meth:`opdm_utils.wikimedia.MediaWikiAPI.parse` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        content = api.parse(
            page="Source_code", prop="wikitext", format="json", redirects=True
        )
        content = json.loads(content)
        self.assertIsNotNone(content)
