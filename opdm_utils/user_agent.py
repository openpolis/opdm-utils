"""User agent module."""

from . import __version__ as pkg_version

import platform


class UserAgent:
    """
    Representation of a user agent.

    A user agent is any software that retrieves, renders and facilitates end user
    interaction with Web content, or whose user interface is implemented using
    Web technologies.

    The *User-Agent* content header is typically sent alongside a http request,
    and allows the server to identify the application type, operating system,
    software vendor or software version of the requesting software.

    The *User-Agent* usually conforms to the following syntax::

        User-Agent: <product>/<product-version> (<comment>)

    Where:

        * ``<product>`` is the identifier of the product.
        * ``<product-version>`` is the version number of the product.
        * ``<comment>`` is string which contains additional information about the product (optional).

    """

    def __init__(
        self, product: str, version: str = pkg_version, comment: str = ""
    ):
        """
        Initialize a user agent.

        :param product: the identifier of the product.
        :param version: the version number of the product. Defaults to the current version of this package.
        :param comment: is string which contains additional information about the product (optional).
        """
        self.product = product
        self.version = version
        self.comment = comment

    def __str__(self):
        """
        Return the string representation of this user agent.

        The string conforms to the typical *User-Agent* content header syntax.

        :return: the string representation of this user agent.
        """
        return f"{self.product}/{self.version}"


def platform_information() -> str:
    """
    Return a string containing information about the running platform enclosed in round brackets.

    Example of possible output::

            "(Linux x86_64)"

    The string is intended to be used as a user agent comment.

    :return: a string containing information about the running platform.
    """
    return f"({platform.system()} {platform.machine()})"


COMMON_USER_AGENT: UserAgent = UserAgent(product="ScrapingTools", comment=platform_information())
"""A generic purpose user agent instance."""
