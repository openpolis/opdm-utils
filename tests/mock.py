# flake8: noqa
_mocked_knowledge_graph_response = {
    "@context": {
        "@vocab": "http://schema.org/",
        "goog": "http://schema.googleapis.com/",
        "EntitySearchResult": "goog:EntitySearchResult",
        "detailedDescription": "goog:detailedDescription",
        "resultScore": "goog:resultScore",
        "kg": "http://g.co/kg",
    },
    "@type": "ItemList",
    "itemListElement": [
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/06c62",
                "name": "Rome",
                "@type": [
                    "Thing",
                    "Place",
                    "AdministrativeArea",
                    "LandmarksOrHistoricalBuildings",
                    "Cemetery",
                    "City",
                ],
                "description": "Capital of Italy",
                "image": {
                    "contentUrl": "http://t3.gstatic.com/images?q=tbn:ANd9GcTyw0Sa7WB6AInOxJjvBooTFTh0w5KXkTiHZTshEt_qJhc-YV7z",
                    "url": "https://commons.wikimedia.org/wiki/File:Trevi_Fountain_Rome_(capital_edit).jpg",
                },
                "detailedDescription": {
                    "articleBody": "Rome is the capital city and a special comune of Italy. Rome also serves as the capital of the Lazio region. With 2,872,800 residents in 1,285 km², it is also the country's most populated comune. ",
                    "url": "https://en.wikipedia.org/wiki/Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.comune.roma.it/",
            },
            "resultScore": 225.280121,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/02l341",
                "name": "Ancient Rome",
                "@type": ["Place", "AdministrativeArea", "Thing"],
                "image": {
                    "contentUrl": "http://t1.gstatic.com/images?q=tbn:ANd9GcRJfvmebo9QPAzPGDXvhcKJIRKpA2GLnVkYcYoeEBPoT2C-qirr",
                    "url": "https://commons.wikimedia.org/wiki/File:Map_of_ancient_Rome.jpg",
                },
                "detailedDescription": {
                    "articleBody": "In historiography, ancient Rome is Roman civilization from the founding of the Italian city of Rome in the 8th century BC to the collapse of the Western Roman Empire in the 5th century AD, encompassing the Roman Kingdom, Roman Republic and Roman Empire until the fall of the western empire.\n",
                    "url": "https://en.wikipedia.org/wiki/Ancient_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 102.943359,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/04jr87",
                "name": "Sapienza University of Rome",
                "@type": [
                    "Thing",
                    "Organization",
                    "EducationalOrganization",
                    "CollegeOrUniversity",
                    "Place",
                ],
                "description": "University in Rome, Italy",
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcTrKyoXz1SL7A6s-h0Vv9VfBQLdfM_0HWD5QgZ1wkP3GRjAJdrz",
                    "url": "https://commons.wikimedia.org/wiki/File:La_Sapienza_Universit%C3%A0_di_Roma.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The Sapienza University of Rome, also called simply Sapienza or the University of Rome, is a collegiate research university located in Rome, Italy. ",
                    "url": "https://en.wikipedia.org/wiki/Sapienza_University_of_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.uniroma1.it",
            },
            "resultScore": 99.190628,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/06cmp",
                "name": "Roman Empire",
                "@type": ["Place", "AdministrativeArea", "Thing", "Country"],
                "description": "Empire",
                "image": {
                    "contentUrl": "http://t0.gstatic.com/images?q=tbn:ANd9GcSDGCNl1FdK0Ope3NVLCp5IBgLfkJfCI5CWYBIk5RJDsL-ncgqX",
                    "url": "https://en.wikipedia.org/wiki/Roman_Syria",
                },
            },
            "resultScore": 74.807121,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/03216t",
                "name": "Sack of Rome",
                "@type": ["Event", "Thing"],
                "description": "Event",
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcTf4qwx6sTivXXA1OvY6LYpkzznLJXBSrOnJ1GNR3aApqDJvGcF",
                    "url": "https://commons.wikimedia.org/wiki/File:The_Sack_of_Rome,_1527._Wellcome_L0003868.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The Sack of Rome on 6 May 1527 was a military event carried out in Rome by the mutinous troops of Charles V, Holy Roman Emperor. ",
                    "url": "https://en.wikipedia.org/wiki/Sack_of_Rome_(1527)",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 70.673141,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/01ky5r",
                "@type": ["Thing", "Place", "Airport", "BusStation"],
                "description": "Airport in Italy",
                "image": {
                    "contentUrl": "http://t1.gstatic.com/images?q=tbn:ANd9GcThwIl75erIp3e2PXwjg1v9OmjD72cvcSjVWhoYpah9ylOCdJ2G",
                    "url": "https://commons.wikimedia.org/wiki/File:Rome_Fiumicino_international_airport_-_Main_entrance_with_streets.jpg",
                },
                "detailedDescription": {
                    "articleBody": 'Rome–Fiumicino International Airport "Leonardo da Vinci", is an international airport in Rome and the major airport in Italy. It is one of the busiest airports in Europe by passenger traffic with almost 43 million passengers served in 2018.\n',
                    "url": "https://en.wikipedia.org/wiki/Leonardo_da_Vinci%E2%80%93Fiumicino_Airport",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.adr.it/",
            },
            "resultScore": 70.553757,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/06k176",
                "name": "Rome",
                "@type": ["TVSeries", "Thing"],
                "description": "American-British-Italian drama series",
                "detailedDescription": {
                    "articleBody": "Rome is a British-American-Italian historical drama television series created by John Milius, William J. MacDonald, and Bruno Heller. The show's two seasons were broadcast on HBO, BBC Two, and Rai 2 between 2005 and 2007. They were later released on DVD and Blu-ray. Rome is set in the 1st century BC, during Ancient Rome's transition from Republic to Empire.\n",
                    "url": "https://en.wikipedia.org/wiki/Rome_(TV_series)",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.hbo.com/rome/",
            },
            "resultScore": 67.935432,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/0lbd9",
                "name": "1960 Summer Olympics",
                "@type": ["Thing", "Event"],
                "description": "Olympic games",
                "image": {
                    "contentUrl": "http://t3.gstatic.com/images?q=tbn:ANd9GcT161W5Z0bw49jnJWgZ5zM4WQFrAq74OyjbmCcuRkum6zxFiR8_",
                    "url": "https://commons.wikimedia.org/wiki/File:Rome_-_Olympic_Stadium_-_1960_Opening_Ceremony.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The 1960 Summer Olympics, officially known as the Games of the XVII Olympiad, was an international multi-sport event that was held from August 25 to September 11, 1960, in Rome, Italy. ",
                    "url": "https://en.wikipedia.org/wiki/1960_Summer_Olympics",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 64.666023,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/08wcn_",
                "name": "Sack of Rome",
                "@type": ["Thing", "Event"],
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcQFeBp41CEUZvSpU36YKnHsKZFvEWDw0MdI2Pyf3-jDe07-yVm8",
                    "url": "https://commons.wikimedia.org/wiki/File:Eroberung_roms_410.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The Sack of Rome occurred on 24 August 410 AD. The city was attacked by the Visigoths led by King Alaric. At that time, Rome was no longer the capital of the Western Roman Empire, having been replaced in that position first by Mediolanum in 286 and then by Ravenna in 402. ",
                    "url": "https://en.wikipedia.org/wiki/Sack_of_Rome_(410)",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 63.740524,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/01xzdz",
                "name": "Pantheon",
                "@type": [
                    "Thing",
                    "TouristAttraction",
                    "Cemetery",
                    "Place",
                    "CivicStructure",
                    "PlaceOfWorship",
                ],
                "description": "Roman temple in Rome, Italy",
                "image": {
                    "contentUrl": "http://t1.gstatic.com/images?q=tbn:ANd9GcQjMbMB0PH9Vl4HwnxvQHsD-RPE359-jOQiavH0hOUe9-1sJI68",
                    "url": "https://commons.wikimedia.org/wiki/File:Rome_Pantheon_front.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The Pantheon is a former Roman temple, now a church, in Rome, Italy, on the site of an earlier temple commissioned by Marcus Agrippa during the reign of Augustus. It was completed by the emperor Hadrian and probably dedicated about 126 AD. ",
                    "url": "https://en.wikipedia.org/wiki/Pantheon,_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.vicariatusurbis.org/Ente.asp?ID=865",
            },
            "resultScore": 61.942745,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/03h1x",
                "name": "Holy See",
                "@type": ["AdministrativeArea", "Place", "Organization", "Thing"],
                "description": "Church jurisdiction",
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcT33CwryZCTYlHBX69h-VqC5AdQChPoQKbD0NGa9HPl3p5Xl3J4",
                    "url": "https://commons.wikimedia.org/wiki/File:Emblem_Holy_See.svg",
                },
                "url": "http://w2.vatican.va/content/vatican/en.html",
            },
            "resultScore": 61.942177,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/02pm0gq",
                "name": "Capture of Rome",
                "@type": ["Thing", "Event"],
                "description": "Event",
                "image": {
                    "contentUrl": "http://t1.gstatic.com/images?q=tbn:ANd9GcTjYNz8BDPFd_zsq8nl1N9Q_AvhUhFD0OikJ8icPN2owmzYgCyO",
                    "url": "https://commons.wikimedia.org/wiki/File:Breccia_di_Porta_Pia_Ademollo.jpg",
                },
                "detailedDescription": {
                    "articleBody": "The capture of Rome, on 20 September 1870 was the final event of the long process of Italian unification known as the Risorgimento, marking both the final defeat of the Papal States under Pope Pius IX and the unification of the Italian peninsula under King Victor Emmanuel II of the House of Savoy.\n",
                    "url": "https://en.wikipedia.org/wiki/Capture_of_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 61.052307,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/098phg",
                "name": "Province of Rome",
                "@type": ["Thing", "Place"],
                "description": "Italian province",
                "image": {
                    "contentUrl": "http://t0.gstatic.com/images?q=tbn:ANd9GcS5yvXep7HnRHxD-YJgI0tnM0X28nUomns8Cu2Cs9Kr6vGwGCBY",
                    "url": "https://en.wikipedia.org/wiki/Province_of_Rome",
                },
                "detailedDescription": {
                    "articleBody": "The Province of Rome was one of the five provinces that formed part of the region of Lazio in Italy. It was established in 1870 and disestablished in 2014. It was essentially coterminous with the Rome metropolitan area. The city of Rome was the provincial capital. During the 1920s, the boundary of the province shrank as land was ceded to establish new provinces. The Province of Rome was the most populous province in Italy. ",
                    "url": "https://en.wikipedia.org/wiki/Province_of_Rome_(1870%E2%80%932014)",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.provincia.roma.it/",
            },
            "resultScore": 58.202496,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/0kdpg",
                "name": "Treaty of Rome",
                "@type": ["Thing", "Event"],
                "detailedDescription": {
                    "articleBody": "The Treaty on the Functioning of the European Union is one of two treaties forming the constitutional basis of the European Union, the other being the Treaty on European Union.\n",
                    "url": "https://en.wikipedia.org/wiki/Treaty_of_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 56.455811,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/06frc",
                "name": "Roman Republic",
                "@type": ["Place", "AdministrativeArea", "Country", "Thing"],
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcQx3dJoouqM58xzDibjjS4HG15g2XKF7C2kDOZ6jPd8iBhqVA8A",
                    "url": "https://en.wikipedia.org/wiki/Roman_Republic",
                },
                "detailedDescription": {
                    "articleBody": "The Roman Republic was the era of classical Roman civilization beginning with the overthrow of the Roman Kingdom, traditionally dated to 509 BC, and ending in 27 BC with the establishment of the Roman Empire. ",
                    "url": "https://en.wikipedia.org/wiki/Roman_Republic",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 54.333958,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/0bxjv",
                "name": "Axis powers",
                "@type": ["Thing"],
                "image": {
                    "contentUrl": "http://t0.gstatic.com/images?q=tbn:ANd9GcTB_eyICxHR5SzQurGUwFe7xkxaRdt55-vTp9Ol0zxhOkGLdOuE",
                    "url": "https://commons.wikimedia.org/wiki/File:Flag_of_the_Axis_Powers_of_America.png",
                },
                "detailedDescription": {
                    "articleBody": 'The Axis powers, also known as "Rome–Berlin–Tokyo Axis", were the nations that fought in World War II against the Allies. The Axis powers agreed on their opposition to the Allies, but did not completely coordinate their activity.\n',
                    "url": "https://en.wikipedia.org/wiki/Axis_powers",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 52.455528,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/06gb8",
                "name": "Roman Kingdom",
                "@type": ["Thing", "Place", "AdministrativeArea", "Country"],
                "description": "Territory",
                "image": {
                    "contentUrl": "http://t1.gstatic.com/images?q=tbn:ANd9GcTA_lr4m1J1-ijfy1KXZuUJu2RxpSFh0wTh7Qk8bLaEcsA1CD-3",
                    "url": "https://en.wikipedia.org/wiki/Roman_Kingdom",
                },
                "detailedDescription": {
                    "articleBody": "The Roman Kingdom, also referred to as the Roman monarchy, or the regal period of ancient Rome, was the earliest period of Roman history, when the city and its territory were ruled by kings.\n",
                    "url": "https://en.wikipedia.org/wiki/Roman_Kingdom",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 50.058601,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/017cw",
                "name": "Byzantine Empire",
                "@type": ["Country", "Place", "AdministrativeArea", "Thing"],
                "description": "Empire",
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcQvNIc7VIgE-qCX30Uqmwhet7W6tt_vXrlivQMCTY47MZbI6at5",
                    "url": "https://commons.wikimedia.org/wiki/File:Byzantine_imperial_flag,_14th_century.svg",
                },
                "detailedDescription": {
                    "articleBody": "The Byzantine Empire, also referred to as the Eastern Roman Empire and Byzantium, was the continuation of the Roman Empire in its eastern provinces during Late Antiquity and the Middle Ages, when its capital city was Constantinople. ",
                    "url": "https://en.wikipedia.org/wiki/Byzantine_Empire",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
            },
            "resultScore": 49.71096,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/04f4trr",
                "name": "Diocese of Rome",
                "@type": ["Thing", "TouristAttraction", "Place"],
                "description": "Catholic church in Rome, Italy",
                "image": {
                    "contentUrl": "http://t2.gstatic.com/images?q=tbn:ANd9GcSZf6XKaawUN4SNRl1JdKda48BzUZXwZUwVH-AqmMhEosBasdbx",
                    "url": "https://en.wikipedia.org/wiki/Diocese_of_Rome",
                },
                "detailedDescription": {
                    "articleBody": "The Diocese of Rome is a diocese of the Catholic Church in Rome. The Bishop of Rome is the Pope, the Supreme Pontiff and leader of the Catholic Church. ",
                    "url": "https://en.wikipedia.org/wiki/Diocese_of_Rome",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.vicariatusurbis.org/",
            },
            "resultScore": 49.675659,
        },
        {
            "@type": "EntitySearchResult",
            "result": {
                "@id": "kg:/m/0jnym",
                "name": "St. Peter's Basilica",
                "@type": [
                    "Place",
                    "Thing",
                    "Cemetery",
                    "PlaceOfWorship",
                    "TouristAttraction",
                    "CivicStructure",
                ],
                "description": "Basilica in Vatican City",
                "image": {
                    "contentUrl": "http://t0.gstatic.com/images?q=tbn:ANd9GcTfsbY8042vkDnBCbTe8njhpNCritP8EgYvwQKT6CY82Ij13yBD",
                    "url": "https://commons.wikimedia.org/wiki/File:0_Basilique_Saint-Pierre_-_Rome_(2).JPG",
                },
                "detailedDescription": {
                    "articleBody": "The Papal Basilica of St. Peter in the Vatican, or simply St. Peter's Basilica, is an Italian Renaissance church in Vatican City, the papal enclave within the city of Rome.\n",
                    "url": "https://en.wikipedia.org/wiki/St._Peter's_Basilica",
                    "license": "https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License",
                },
                "url": "http://www.vatican.va/various/basiliche/san_pietro/index_it.htm",
            },
            "resultScore": 49.161537,
        },
    ],
}
