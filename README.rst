==========
OPDM Utils
==========

A collection of various utilities.

Features
--------

    * Disambiguation and gender guessing of person names.
    * Google Knowledge Graph API micro-client.
    * MediaWiki API micro-client.
    * 100% test coverage.
    * 100% type annotated codebase.
    * Free software (GPLv3)

License
-------

Copyright (C) 2019 `fondazione Openpolis`_

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or many later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/.

Credits
-------

This package was kick-started using Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
.. _`Fondazione openpolis`: https://www.openpolis.it/fondazione/chi-siamo/
