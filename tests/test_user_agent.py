"""Module containing tests for :py:mod:`opdm_utils.user_agent` module."""


import unittest
import re

from opdm_utils import user_agent


class TestUserAgent(unittest.TestCase):
    """Test class for :py:mod:`opdm_utils.user_agent` module."""

    def test_str(self):
        """Test if the string representation of the user agent is valid."""
        ua = user_agent.UserAgent("Test")
        self.assertIsNotNone(re.search(r".+?[/\s][\d.]+", str(ua)))
