"""A collection of classes and functions easing the interaction with a MediaWiki API."""
from typing import Dict, Any, Union, List

import requests

from opdm_utils.user_agent import UserAgent


class MediaWikiAPI:
    """
    Simple micro-client for MediaWiki API.

    Provides a statically type-checked Python API to a MediaWiki endpoint.

    Examples of existing MediaWiki API Endpoints:

        *  `MediaWiki API <https://www.mediawiki.org/w/api.php>`_
        *  `English Wikipedia API  <https://en.wikipedia.org/w/api.php>`_
        *  `Italian Wikipedia API  <https://it.wikipedia.org/w/api.php>`_
        *  `Wikimedia Commons API  <https://commons.wikimedia.org/w/api.php>`_

    For more information, visit the `official documentation of the MediaWiki API
    <https://www.mediawiki.org/wiki/API:Main_page>`_.

    .. Links:
    .. _`action=query`: https://www.mediawiki.org/wiki/API:Query
    .. _`action=parse`: https://www.mediawiki.org/wiki/API:Parse

    """

    user_agent = UserAgent("MediaWikiClient")
    """The user agent that will be set in the request header."""

    def __init__(self, endpoint: str):
        """
        Initialize a MediaWiki API client.

        :param endpoint: URL pointing to a MediaWiki API instance.
        """
        self.end_point = endpoint

    def get(self, action: str, **params: Union[str, int]) -> Any:
        """
        Send a generic get request with arbitrary parameters to a MediaWiki API.

        :param action:
            Which action to perform.
            Should be one of the following values:

                * ``query``: Fetch data from and about MediaWiki.
                * ``parse``: Parse content and return parser output.

        :param params: Any additional parameters to be appended to the request URL.
        :return:
        """
        params = self._set_defaults(action=action, **params)
        headers = {"User-Agent": str(self.user_agent)}
        url = f"{self.end_point}?"
        response = requests.get(url, params=params, headers=headers)
        return response.content

    def query(
        self,
        prop: Union[str, List[str]] = None,
        list_: Union[str, List[str]] = None,
        meta: Union[str, List[str]] = None,
        titles: Union[str, List[str]] = None,
        pageids: Union[int, List[int]] = None,
        revids: Union[int, List[int]] = None,
        generator: str = None,
        redirects: bool = None,
        converttitles: bool = None,
        **params: Union[str, int],
    ) -> Any:
        """
        Fetch data from and about MediaWiki.

        Implements `action=query`_ API module.

        Examples:
            * Fetch site info and revisions of Main Page:
                .. code-block:: python

                    query(prop="revisions", meta="siteinfo", titles="Main Page", rvprop="user|comment", continue=True)

            * Fetch revisions of pages beginning with API:
                .. code-block:: python

                    query(generator="allpages", gapprefix="API", prop="revisions",)

        :param prop: Which properties to get for the queried pages.
        :param list_: Which lists to get.
        :param meta: Which metadata to get.
        :param titles: A list of titles to work on.
        :param pageids: A list of page IDs to work on.
        :param revids: A list of revision IDs to work on.
        :param generator: Get the list of pages to work on by executing the specified query module.
        :param redirects: Automatically resolve redirects in `query+titles`, `query+pageids`, and `query+revids`, and
            in pages returned by `query+generator`.
        :param converttitles: Convert titles to other variants if necessary.
            Only works if the wiki's content language supports variant conversion.
            Languages that support variant conversion include en, crh, gan, iu, kk, ku, shi, sr, tg, uz and zh.
        :param params: Any additional parameters to be appended to the request URL.

        :return: the content of the response.

        """
        return self.get(
            action="query",
            **self._process_params(
                generator=generator,
                redirects=redirects,
                converttitles=converttitles,
                list=list_,
                meta=meta,
                titles=titles,
                revids=revids,
                pageids=pageids,
                prop=prop,
                **params,
            ),
        )

    def parse(
        self,
        title: str = None,
        text: str = None,
        revid: int = None,
        contentmodel: str = None,
        page: str = None,
        pageid: int = None,
        oldid: int = None,
        redirects: bool = None,
        prop: str = None,
        **params: Union[str, int],
    ) -> Any:
        """
        Parse content and return parser output.

        Implements `action=parse`_ API module.

        See the various prop-modules of `action=query`_ to get information from the current version of a page.

        There are several ways to specify the text to parse:

            1. Specify a page or revision, using `page`, `pageid`, or `oldid`.
            2. Specify content explicitly, using `text`, `title`, `revid`, and `contentmodel`.
            3. Specify only a summary to parse. `prop` should be given an empty value.

        :param page: Title of page the text belongs to. If omitted, `contentmodel` must be specified, and API will be
            used as the title.
        :param pageid: Parse the content of this page. Overrides `page`.
        :param oldid: Parse the content of this revision. Overrides `page` and `pageid`.
        :param text: Text to parse. Use `title` or `contentmodel` to control the content model.
        :param title: Title of page the text belongs to. If omitted, `contentmodel` must be specified, and API will be
            used as the title.
        :param revid: Revision ID.
        :param contentmodel: Content model of the input text. If omitted, title must be specified, and default will be
            the model of the specified title. Only valid when used with `text`.
            One of the following values:

                * ``GadgetDefinition``
                * ``SecurePoll``
                * ``sanitized-css``
                * ``MassMessageListContent``
                * ``flow-board``
                * ``Scribunto``
                * ``JsonSchema``
                * ``NewsletterContent``
                * ``wikitext``
                * ``javascript``
                * ``json``
                * ``css``
                * ``text``

        :param redirects: If page or pageid is set to a redirect, resolve it.
        :param prop:
            Which pieces of information to get:

                * ``text``:
                    Gives the parsed text of the wikitext.
                * ``langlinks``:
                    Gives the language links in the parsed wikitext.
                * ``categories``:
                    Gives the categories in the parsed wikitext.
                * ``categorieshtml``:
                    Gives the HTML version of the categories.
                * ``links``:
                    Gives the internal links in the parsed wikitext.
                * ``templates``:
                    Gives the templates in the parsed wikitext.
                * ``images``:
                    Gives the images in the parsed wikitext.
                * ``externallinks``:
                    Gives the external links in the parsed wikitext.
                * ``sections``:
                    Gives the sections in the parsed wikitext.
                * ``revid``:
                    Adds the revision ID of the parsed page.
                * ``displaytitle``:
                    Adds the title of the parsed wikitext.
                * ``headhtml``:
                    Gives parsed doctype, opening ``<html>``, ``<head>`` element and opening ``<body>`` of the page.
                * ``modules``:
                    Gives the ResourceLoader modules used on the page. To load, use mw.loader.using().
                    Either ``jsconfigvars`` or ``encodedjsconfigvars`` must be requested jointly with modules.
                * ``jsconfigvars``:
                    Gives the JavaScript configuration variables specific to the page.
                    To apply, use mw.config.set().
                * ``encodedjsconfigvars``:
                    Gives the JavaScript configuration variables specific to the page as a JSON string.
                * ``indicators``:
                    Gives the HTML of page status indicators used on the page.
                * ``iwlinks``:
                    Gives interwiki links in the parsed wikitext.
                * ``wikitext``:
                    Gives the original wikitext that was parsed.
                * ``properties``:
                    Gives various properties defined in the parsed wikitext.
                * ``limitreportdata``:
                    Gives the limit report in a structured way.
                    Gives no data, when ``disablelimitreport`` is set.
                * ``limitreporthtml``:
                    Gives the HTML version of the limit report.
                    Gives no data, when ``disablelimitreport`` is set.
                * ``parsetree``:
                    The XML parse tree of revision content (requires content model wikitext).
                * ``parsewarnings``:
                    Gives the warnings that occurred while parsing content.

        :param params: Any additional parameters to be appended to the request URL.

        :return: the content of the response.
        """
        return self.get(
            action="parse",
            **self._process_params(
                page=page,
                pageid=pageid,
                oldid=oldid,
                title=title,
                text=text,
                revid=revid,
                contentmodel=contentmodel,
                redirects=redirects,
                prop=prop,
                **params,
            ),
        )

    @classmethod
    def _set_defaults(cls, **params: Union[str, int]) -> Dict[str, Union[str, int]]:
        """
        Set default values of MediaWiki API parameters if not already set.

        :param params:
        :return:
        """
        params["format"] = params.get("format", "jsonfm")

        return params

    @classmethod
    def _process_params(cls, **params: Any) -> Dict[str, Union[str, int]]:
        """
        Process parameters in a way that can be digested by a MediaWiki API.

        :param params: the parameters to be processed
        :return: a dictionary having the parameter name as key and its value as value.
        """
        return {
            key: (cls._separate_values(value) if isinstance(value, List) else value)
            for key, value in params.items()
            if value is not None
        }

    @classmethod
    def _separate_values(cls, param: List[Union[int, str]]) -> str:
        """
        Join a list of values into a single parameter string, separating them with "|" character.

        :param param: the list of values to be merged.
        :return: a string composed by the elements of the list joined together and separated with "|" character.
        """
        return "|".join(map(str, param))
