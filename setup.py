#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

with open("requirements.txt") as requirements_file:
    requirements = requirements_file.read().splitlines()

with open("requirements_dev.txt") as requirements_dev_file:
    dev_requirements = requirements_dev_file.read().splitlines()

with open("requirements_test.txt") as requirements_test_file:
    test_requirements = requirements_test_file.read().splitlines()

lint_requirements = list(filter(lambda x: x.startswith("flake8"), dev_requirements))


def long_description():
    """Generate package full description."""
    return f"{readme}\n{history}"


setup(
    author="Gabriele Lucci",
    author_email="gabriele@openpolis.it",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Internet :: WWW/HTTP",
    ],
    python_requires=">=3.6",
    description="Collection of utilities facilitating web scraping operations",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=long_description(),
    include_package_data=True,
    keywords="opdm_utils",
    name="opdm_utils",
    packages=find_packages(include=["opdm_utils"]),
    test_suite="tests",
    extras_require={
        "dev": dev_requirements,
        "lint": lint_requirements,
        "test": test_requirements,
    },
    tests_require=test_requirements,
    url="https://gitlab.depp.it/openpolis/opdm-utils",
    version="0.3.1",
    zip_safe=False,
)
