=======
Credits
=======

Development Lead
----------------

* Gabriele Lucci <gabriele@openpolis.it>

Contributors
------------

* Ettore Di Cesare <ettore@openpolis.it>
