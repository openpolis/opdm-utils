"""A collection of functions which do some kind of disambiguation of person names."""
from typing import Dict, Optional, List

from .data import NAMES

family_name_prefixes = [
    "DI",
    "DE",
    "DEL",
    "LA",
    "LE",
    "LI",
    "LO",
    "D'",
    "DA",
    "DAL",
    "DE'",
    "AL",
    "VON",
    "Ó",
    "O'",
]


def lookup_given_name(given_name: str) -> int:
    """
    Lookup a given name.

    :param given_name: the given name to be looked for.
    :return: the frequency of the given name. Return 0 if it wasn't found.
    """
    return sum(NAMES.get(given_name.upper(), []))


def guess_gender(given_name: str) -> str:
    """
    Try to guess the gender of the specified given name.

    Raise a :py:exc:`NameNotFoundException` if the name was not found in the lookup dictionary
    (:py:data:`opdm_utils.data.NAMES`).

    :param given_name: the given name to be guessed.
    :return: a string containing a single character. The character would be ``"M"`` if the guessed gender is *male*,
        ``"F"`` if it is *female*.
    :raises: NameNotFoundException: if the given name is not found.
    """
    found = NAMES.get(given_name.upper())
    if found:
        m, f = found
        if f > m:
            return "F"
        else:
            return "M"
    else:
        raise NameNotFoundException(given_name)


def disambiguate(full_name: str, invert: bool = False) -> Dict[str, Optional[str]]:
    """
    Disambiguate a string containing a given name and a family name.

    Given a set of names, try to guess which part is a given name and which is a
    family name, while also guessing the gender of the given name.

    By default, assume the given name comes first (e.g. ``<given_name family_name>``).

    If you want to get the gender of just a given name (without family name),
    you really should use to simpler :py:func:`guess_gender`.

    This implementation is purposefully naive and based on "empirical" observations,
    surely far from perfect and by no means "scientific".
    It's purely meant to be good enough and to work *most* of the times on a set
    of simple and quite specific cases (e.g. italian names).

    :param full_name:
    :param invert: True if the ``<family name>`` comes first (before the ``<given name>``). Defaults to ``False``
    :return:
        A dictionary assigning given name, family name, other names and gender to the keys ``given_name``,
        ``family_name``, ``other_names``, ``gender``.

    """
    tmp_given_name = tmp_family_name = tmp_other_names = ""
    bits: List[str] = full_name.strip().split()
    if len(bits) == 1:
        tmp = bits[0]
        if lookup_given_name(tmp):
            tmp_given_name = tmp
        else:
            tmp_other_names = tmp
    # Case: full_name is composed by <family_name given_name> (family name first)
    elif invert:
        # Assume the first bit is (at least a part of) the family_name
        tmp_family_name = bits.pop(0)
        tmp_given_name = ""
        tmp_other_names = ""
        # Look for possible family name prefix
        if tmp_family_name.upper() in family_name_prefixes:
            tmp_family_name = " ".join([tmp_family_name, bits.pop(0)])
        while bits:
            tmp_given_name = " ".join(bits)
            if lookup_given_name(tmp_given_name):
                break
            else:
                # Pop the rightmost bit and put and consider it part of the middle name
                tmp_other_names = " ".join([bits.pop(), tmp_other_names]).strip()
    # Case: full_name is composed by <given_name family_name> (given name first)
    else:
        # Assume the last bit is (at least a part of) the family_name
        tmp_family_name = bits.pop()
        tmp_given_name = ""
        tmp_other_names = ""
        # Look for possible family name prefix
        if bits[-1].upper() in family_name_prefixes:
            tmp_family_name = " ".join([bits.pop(), tmp_family_name])
        while bits:
            tmp_given_name = " ".join(bits)
            if lookup_given_name(tmp_given_name):
                break
            else:
                # Pop the rightmost bit and put and consider it part of the middle name
                tmp_other_names = " ".join([bits.pop(), tmp_other_names]).strip()

    # Finally, try to guess the gender of the supposed given name and normalize
    gender: str = guess_gender(tmp_given_name) if tmp_given_name else None
    given_name: str = tmp_given_name if tmp_given_name else None
    family_name: str = tmp_family_name if tmp_family_name else None
    other_names: str = tmp_other_names if tmp_other_names else None

    return {
        "given_name": given_name,
        "family_name": family_name,
        "other_names": other_names,
        "gender": gender,
    }


class NameNotFoundException(Exception):
    """Exception thrown when a name is not found in the keys of :py:data:`opdm_utils.data.NAMES`."""

    def __init__(self, name):
        self.name = name
        super().__init__(f"Lookup for name {name} failed")
