"""Module containing tests for :py:mod:`opdm_utils.wikimedia` module."""

import unittest
from unittest.mock import patch

from opdm_utils import wikimedia

ENDPOINT = "http://www.example.wiki/w/api.php"


def _mocked_requests_get(*args, **kwargs):
    class MockedResponse:
        def __init__(self, content, status_code):
            self.content = content
            self.status_code = status_code

        def json(self):
            return self.content

    return MockedResponse("sample content", 200)


class TestMediaWikiAPI(unittest.TestCase):
    """Test class for :py:class:`opdm_utils.wikimedia.MediaWikiAPI` class."""

    @patch("requests.get", side_effect=_mocked_requests_get)
    def test_get(self, *args):
        """Test the :py:meth:`opdm_utils.google.MediaWikiAPI.get` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        content = api.get(
            action="query",
            prop="revisions",
            meta="siteinfo",
            titles="Main Page",
            rvprop="user|comment",
        )
        self.assertIsNotNone(content)

    @patch("requests.get", side_effect=_mocked_requests_get)
    def test_query(self, *args):
        """Test the :py:meth:`opdm_utils.google.MediaWikiAPI.query` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        content = api.query(
            prop="revisions", meta="siteinfo", titles="Main Page", rvprop="user|comment"
        )
        self.assertIsNotNone(content)

    @patch("requests.get", side_effect=_mocked_requests_get)
    def test_parse(self, *args):
        """Test the :py:meth:`opdm_utils.google.MediaWikiAPI.parse` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        content = api.parse(
            page="Source_code", prop="wikitext", format="json", redirects=True
        )
        self.assertIsNotNone(content)

    def test__process_params(self):
        """Test the :py:meth:`opdm_utils.google.MediaWikiAPI._process_params` method."""
        api = wikimedia.MediaWikiAPI(endpoint=ENDPOINT)
        params = {
            "prop": "revisions",
            "meta": "siteinfo",
            "titles": "Main Page",
            "rvprop": ["user", "comment"],
            "continue": True,
        }
        result = api._process_params(**params)
        self.assertEqual(result["rvprop"].split("|"), params["rvprop"])
