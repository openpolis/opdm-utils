opdm\_utils package
===================

Submodules
----------

opdm\_utils.data module
-----------------------

.. automodule:: opdm_utils.data
    :members:
    :undoc-members:
    :show-inheritance:

opdm\_utils.disambiguate module
-------------------------------

.. automodule:: opdm_utils.disambiguate
    :members:
    :undoc-members:
    :show-inheritance:

opdm\_utils.google module
-------------------------

.. automodule:: opdm_utils.google
    :members:
    :undoc-members:
    :show-inheritance:

opdm\_utils.user\_agent module
------------------------------

.. automodule:: opdm_utils.user_agent
    :members:
    :undoc-members:
    :show-inheritance:

opdm\_utils.wikimedia module
----------------------------

.. automodule:: opdm_utils.wikimedia
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opdm_utils
    :members:
    :undoc-members:
    :show-inheritance:
