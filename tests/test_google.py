"""Module containing tests for :py:mod:`opdm_utils.google` module."""

import unittest

from unittest.mock import patch

from opdm_utils import google
from tests.mock import _mocked_knowledge_graph_response


def _mocked_requests_get(*args, **kwargs):
    class MockedResponse:
        def __init__(self, data, status_code):
            self.data = data
            self.status_code = status_code

        def json(self):
            return self.data

    return MockedResponse(_mocked_knowledge_graph_response, 200)


class TestKnowledgeGraphSearchAPI(unittest.TestCase):
    """Test class for :py:class:`opdm_utils.google.KnowledgeGraphSearchAPI` class."""

    @patch("requests.get", side_effect=_mocked_requests_get)
    def test_search(self, mocked_get):
        """Test the :py:meth:`opdm_utils.google.KnowledgeGraphSearchAPI.search` method."""
        api = google.KnowledgeGraphSearchAPI(api_key="api_key")
        results = api.search(query="Roma")
        self.assertIsInstance(results, dict)  # should yield a dict
        self.assertIn("itemListElement", results)
        self.assertIsInstance(results["itemListElement"], list)
