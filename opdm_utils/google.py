"""A collection of classes and functions easing the interaction with Google services."""
from typing import Any, Dict

import requests

from opdm_utils.user_agent import UserAgent


class KnowledgeGraphSearchAPI:
    """Simple micro-client for Google Knowledge Graph API (v1)."""

    url = "https://kgsearch.googleapis.com"
    """The base URL of Google Knowledge Graph API."""

    api_version = "v1"
    """The version of the Google Knowledge Graph API."""

    user_agent = UserAgent("KnowledgeGraphSearchAPI")
    """The user agent that will be set in the request header."""

    def __init__(self, api_key: str):
        """
        Initialize a minimal client for Google Knowledge Graph API.

        :param api_key: the API key to be used for requests (required).
        """
        self.api_key = api_key

    def search(self, **params) -> Dict[str, Any]:
        """
        Search Knowledge Graph for entities that match the constraints.

        Return a ranked list of the most notable entities that match the specified query.

        Usage::

            api = google.KnowledgeGraphSearchAPI(api_key="your-api-key")
            results = api.search(query="Roma")


        The method accepts the same parameters of the entities.search_ endpoint.

        .. _`entities.search`: https://developers.google.com/knowledge-graph/reference/rest/v1/

        :`params`:
            * **key** (``str``) --
                The API key to be used for the request (overrides the default api_key).
            * **query** (``str``) --
                A literal string to search for in the Knowledge Graph.
            * **ids** (``str``) --
                A list of entity IDs to search for in the Knowledge Graph. Multiple IDs can be specified.
            * **types** (``str``) --
                 Restricts returned entities to those of the specified types. For example, you can specify
                 `Person <http://schema.org/Person>`_ to restrict the results to entities representing people.
                 If multiple types are specified, returned entities will contain one or more of these types.
            * **language** (``str``) --
                The list of language codes (defined in `ISO 639 <https://www.iso.org/iso-639-language-codes.html>`_) to
                run the query with, for instance `en`.
            * **limit** (``int``) --
                Limits the number of entities to be returned.
            * **indent** (``bool``) --
                Enables indenting of JSON results.
            * **prefix** (``bool``) --
                Enables prefix (initial substring) match against names and aliases of entities. For example, a prefix
                `Jung` will match entities and aliases such as `Jung`, `Jungle`, and `Jung-ho Kang`.
        :return: a ranked list of the most notable entities that match the specified query.
        """
        params["key"] = params.get("key", self.api_key)
        headers = {"User-Agent": str(self.user_agent)}
        url = f"{self._get_service_url()}/entities:search"
        response = requests.get(url, params=params, headers=headers)
        content = response.json()
        return content

    def _get_service_url(self) -> str:
        """
        Compose and return the service url.

        :return: the service url.
        """
        return f"{self.url}/{self.api_version}"
