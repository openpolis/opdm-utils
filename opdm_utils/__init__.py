# -*- coding: utf-8 -*-

"""Top-level package for OPDM Utils."""

__author__ = """Gabriele Lucci"""
__email__ = "gabriele@openpolis.it"
__version__ = "0.3.1"
