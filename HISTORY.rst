================================================================================
                                    History
================================================================================

All notable changes to this project will be documented in this file.

The format is inspired by `Keep a Changelog`_,
and this project adheres to `Semantic Versioning`_.

..
    --------------------------------------------------------------------------------
    Unreleased_ -
    --------------------------------------------------------------------------------


--------------------------------------------------------------------------------
0.3.1_ - 2019-07-23
--------------------------------------------------------------------------------

Changed
-------

* Rename package from "Scraping Tools" to "opdm-utils".
* `disambiguate.disambiguate()` function now returns a dictionary with `None`
    instead of empty string values.
* `google.KnowledgeGraphSearchAPI.search()` now returns exactly what the http API
    returns; doesn't process the response anymore. This allows the catching and
    handling of errors that may occur if the request fails for any reason.

--------------------------------------------------------------------------------
0.3.0_ - 2019-04-09
--------------------------------------------------------------------------------

Added
-----

* `MediaWikiAPI` micro-client for MediaWiki API
  (supports query and parse actions).
* `user_agent` module containing various utilities.
* API clients now use a custom user agent in request header.

--------------------------------------------------------------------------------
0.2.1_ - 2019-04-03
--------------------------------------------------------------------------------

Fixed
-----

* Fixed a typo which was causing the search method to fail.

--------------------------------------------------------------------------------
0.2.0_ - 2019-04-03
--------------------------------------------------------------------------------

Added
-----

* `KnowledgeGraphSearchAPI` micro-client for Google `Knowledge Graph API`_.

--------------------------------------------------------------------------------
0.1.0_ - 2019-03-27
--------------------------------------------------------------------------------

First release.

Added
-----

* New features will be listed here.

Changed
-------

* Changes in existing functionality will be listed here.

Deprecated
----------

* Soon-to-be removed features will be listed here.

Removed
-------

* Removed features will be listed here.

Fixed
-----

* Bug fixes will be listed here.

Security
--------

* Vulnerabilities and security-related fixes will be listed here

.. External links:
.. _Unreleased: https://gitlab.depp.it/openpolis/opdm-utils/compare/v0.3.0...master
.. _0.3.0: https://gitlab.depp.it/openpolis/opdm-utils/compare/v0.3.0...v0.3.1
.. _0.3.0: https://gitlab.depp.it/openpolis/opdm-utils/compare/v0.2.1...v0.3.0
.. _0.2.1: https://gitlab.depp.it/openpolis/opdm-utils/compare/v0.2.0...v0.2.1
.. _0.2.0: https://gitlab.depp.it/openpolis/opdm-utils/compare/v0.1.0...v0.2.0
.. _0.1.0: https://gitlab.depp.it/openpolis/opdm-utils/compare/55ed1012a51835000bea280cd3fe330abb7a9a99...v0.1.0
.. _`Keep a Changelog`: https://keepachangelog.com/en/1.0.0/
.. _`Semantic Versioning`: https://semver.org/spec/v2.0.0.html
.. _`Knowledge Graph API`: https://developers.google.com/knowledge-graph/reference/rest/v1/
