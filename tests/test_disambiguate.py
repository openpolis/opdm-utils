"""Module containing tests for :py:mod:`opdm_utils.disambiguate` module."""


import unittest

from opdm_utils import disambiguate

INVALID_NAME = "___non_existing_name___"

NAMES_POOL = (
    ("Gabriele", "Lucci", "M"),
    ("Maria Giovanna", "Di Stefano", "F"),
    ("Vilfredo Federico Damaso", "Pareto", "M"),
    ("Giovannino", "Neri", "M"),
    ("Alfredo", "De Rossi", "M"),
    ("Martina", "Bianchi", "F"),
    ("Martino", "Rossi", "M"),
    ("Veronica", "Verdi", "F"),
    ("Nicola", "Del Nero", "M"),
    ("Nicoletta Sabrina", "Del Monte", "F"),
    ("Niccolò", "Contessa", "M"),
)


class TestGuessGender(unittest.TestCase):
    """Test class for :py:mod:`opdm_utils.disambiguate` module."""

    def test_guess_gender(self):
        """Test :py:func:`guess_gender` function."""
        names = [(n[0], n[2]) for n in NAMES_POOL]
        for given_name, gender in names:
            try:
                self.assertEqual(disambiguate.guess_gender(given_name), gender)
            except disambiguate.NameNotFoundException:
                pass

    def test_guess_gender_ERR(self):
        """Test :py:func:`guess_gender` function."""
        self.assertRaises(
            disambiguate.NameNotFoundException, disambiguate.guess_gender, INVALID_NAME
        )

    def test_disambiguate(self):
        """Test :py:func:`disambiguate` function."""
        for given_name, family_name, gender in NAMES_POOL:
            full_name = f"{given_name} {family_name}"
            with self.subTest():
                ret = disambiguate.disambiguate(full_name)
                self.assertEqual(ret["gender"], gender)
                self.assertEqual(ret["family_name"], family_name)
                self.assertIn(ret["given_name"], given_name)

    def test_disambiguate_inverse(self):
        """Test :py:func:`disambiguate` function."""
        for given_name, family_name, gender in NAMES_POOL:
            full_name = f"{family_name} {given_name}"
            with self.subTest():
                ret = disambiguate.disambiguate(full_name, True)
                self.assertEqual(ret["gender"], gender)
                self.assertEqual(ret["family_name"], family_name)
                self.assertIn(ret["given_name"], given_name)

    def test_disambiguate_single_word(self):
        """Test :py:func:`disambiguate` function."""
        given_name = "Gabriele"
        family_name = ""
        gender = "M"
        full_name = f"{given_name} {family_name}"

        ret = disambiguate.disambiguate(full_name, True)
        self.assertEqual(ret["gender"], gender)
        self.assertIsNone(ret["family_name"])
        self.assertIn(ret["given_name"], given_name)
