"""Integration tests for :py:mod:`opdm_utils.google` module."""

import os
import unittest

from opdm_utils import google

api_key = os.environ.get("GOOGLE_API_KEY")


@unittest.skipIf(api_key is None, "GOOGLE_API_KEY not set")
class TestKnowledgeGraphSearchAPIWithIntegration(unittest.TestCase):
    """
    Test class for :py:class:`opdm_utils.google.KnowledgeGraphSearchAPI` class.

    These tests are made against the actual Google Knowledge Graph REST API.

    You must set the environment variable "GOOGLE_API_KEY in order to run these tests.

    If the environment variable "GOOGLE_API_KEY" is set but contains an invalid API key, the tests will fail.
    """

    def test_search(self):
        """Test the :py:meth:`opdm_utils.google.KnowledgeGraphSearchAPI.search` method."""
        api = google.KnowledgeGraphSearchAPI(api_key=api_key)
        results = api.search(query="Roma")
        self.assertIsInstance(results, dict)  # should yield a dict
        self.assertIn("itemListElement", results)
        self.assertIsInstance(results["itemListElement"], list)

    def test_search_bad_request(self):
        """Test the :py:meth:`opdm_utils.google.KnowledgeGraphSearchAPI.search` method."""
        api = google.KnowledgeGraphSearchAPI(api_key=api_key)
        results = api.search(query="Roma", asd="bad")
        self.assertIsInstance(results, dict)
        self.assertIn("error", results)
        self.assertIsInstance(results["error"], dict)
        self.assertIn("code", results["error"])
        self.assertEquals(results["error"]["code"], 400)
