"""Module containing tests for :py:mod:`opdm_utils.data` module."""


import unittest

from opdm_utils import data


class TestData(unittest.TestCase):
    """Test class for :py:mod:`opdm_utils.data` module."""

    def test_NAMES(self):
        """Test if the dictionary is structured properly."""
        self.assertIsInstance(data.NAMES, dict)
        for name, score in data.NAMES.items():
            self.assertIsInstance(name, str)
            self.assertIsInstance(score, tuple)
            self.assertGreater(sum(score), 0)
